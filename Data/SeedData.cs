using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using BananaIncBooking.Models;
using System;
namespace BananaIncBooking
{
    public class SeedData
    {

        public static void SeedDatabase(BookingContext context)
        {
            if (context.Database.GetMigrations().Count() > 0
                    && context.Database.GetPendingMigrations().Count() == 0
                    && context.Movies.Count() == 0)
            {
                // Seed data:

                // Movies:
                var movies = new List<Movie>{
                 new Movie
                {
                    title = "AMAZING GRACE",
                    genre = "Documentary",
                    poster_url = "https://resizing.flixster.com/H2l0DQAjzYdjHYjFhGf-uEuW1mM=/fit-in/200x296.2962962962963/v1.bTsxMzAxNDI3MztqOzE4MTUwOzEyMDA7MjAwMDsyOTYz",
                    description = "Concert footage from 1972 of Aretha Franklin performing songs from the best-selling gospel album at the New Temple Missionary Baptist Church in Los Angeles.",
                    age_restriction = "G"

                },
                new Movie
                {
                    title = "TOY STORY 4",
                    genre = "Animation",
                    poster_url = "https://resizing.flixster.com/zsLA1Duau__OkCWWfWBZkL2Gi1Q=/fit-in/200x296.2962962962963/v1.bTsxMzAyMzkxNjtqOzE4MTUwOzEyMDA7MTA4NjsxNjA5",
                    description = "Woody (voice of Tom Hanks) has always been confident about his place in the world, and that his priority is taking care of his kid, whether that's Andy or Bonnie. So when Bonnie's beloved new craft-project-turned-toy, Forky (voice of Tony Hale), declares himself as 'trash' and not a toy, Woody takes it upon himself to show Forky why he should embrace being a toy. But when Bonnie takes the whole gang on her family's road trip excursion, Woody ends up on an unexpected detour that includes a reunion with his long-lost friend Bo Peep (voice of Annie Potts). After years of being on her own, Bo's adventurous spirit and life on the road belie her delicate porcelain exterior. As Woody and Bo realize they're worlds apart when it comes to life as a toy, they soon come to find that's the least of their worries.",
                    age_restriction = "G"

                },
                new Movie
                {
                    title = "3 FACES",
                    genre = "Drama",
                    poster_url = "https://resizing.flixster.com/FNMAPtcUMZ5Av_eOlIm9PplIl2g=/fit-in/200x296.2962962962963/v1.bTsxMjk3NDA0NTtqOzE4MTUwOzEyMDA7MjAyNTszMDAw",
                    description = "Well-known actress Behnaz Jafari is distraught by a provincial girl's video plea for help--oppressed by her family to not pursue her studies at the Tehran drama conservatory. Behnaz abandons her shoot and turns to filmmaker Jafar Panahi to help solve the mystery of the young girl's troubles. They travel by car to the rural northwest where they have amusing encounters with the charming folk of the girl's mountain village. But the city visitors soon discover that the protection of age-old traditions is as generous as local hospitality...",
                    age_restriction = "NR"

                },
                new Movie
                {
                    title = "APOLLO 11",
                    genre = "Documentary",
                    poster_url = "https://resizing.flixster.com/Asj5SAh7MXZ3oxHZWKEVtxT2P1U=/fit-in/200x296.2962962962963/v1.bTsxMjk3ODk2MjtqOzE4MTUwOzEyMDA7ODEwOzEyMDA",
                    description = "Apollo 11 is a cinematic space event film fifty years in the making. Featuring never-before-seen large-format film footage of one of humanity's greatest accomplishments.",
                    age_restriction = "G"

                },
                new Movie
                {
                    title = "ASH IS PUREST WHITE",
                    genre = "Romance",
                    poster_url = "https://resizing.flixster.com/uxa-a16_zN18fSslh27PRgwOKNk=/fit-in/200x296.2962962962963/v1.bTsxMjk4MDMxNDtqOzE4MTUwOzEyMDA7ODA5NTsxMTk5OA",
                    description = "A tragicomedy initially set in the jianghu-criminal underworld-setting, ASH IS PUREST WHITE is less a gangster movie than a melodrama. With a three-part structure, it begins by following the quick-witted Qiao (Tao Zhao) and her mobster boyfriend Bin (Fan Liao) as they stake out their turf against rivals and upstarts in 2001 postindustrial Datong before expanding out into an epic narrative of how abstract forces shape individual lives, and continues Jia Zhangke's body of work as a record of 21st-century China and its warp-speed transformations.",
                    age_restriction = "NR"

                },
                new Movie
                {
                    title = "THE HEIRESSES (LAS HEREDERAS)",
                    genre = "Drama",
                    poster_url = "https://resizing.flixster.com/RIDcT1JuVzD31FppOPxk-hCWXL8=/206x305/v1.bTsxMjg5NzMxODtqOzE4MTQ5OzEyMDA7ODE0OTsxMjA0Nw",
                    description = "Chela and Chiquita, both descended from wealthy families in Asunción, Paraguay, have been together for over 30 years. But recently their financial situation has worsened and they begin selling off their inherited possessions. But when their debts lead to Chiquita being imprisoned on fraud charges, Chela is forced to face a new reality. Driving for the first time in years, she begins to provide a local taxi service to a group of elderly wealthy ladies. As Chela settles into her new life, she encounters the much younger Angy, forging a fresh and invigorating new connection. Chela finally begins to break out of her shell and engage with the world, embarking on her own personal, intimate revolution.",
                    age_restriction = "NR"

                },
                new Movie
                {
                    title = "LITTLE WOODS",
                    genre = "Drama",
                    poster_url = "https://resizing.flixster.com/HCfbyqbaEojezzil6Mgb5tynZd8=/fit-in/200x296.2962962962963/v1.bTsxMjk2Mzk1NztwOzE4MTUwOzEyMDA7NDE3OzYxNA",
                    description = "Little Woods, North Dakota, a fracking boomtown well beyond its prime. Ollie is trying to survive the last few days of her probation after getting caught illegally running prescription pills over the Canadian border. But when her mother dies, she is thrust back into the life of her estranged sister Deb (James), who is facing her own crisis with an unplanned pregnancy and a deadbeat ex. The two find they have one week to settle the mortgage on their mother's house or face foreclosure. As bills and pressure mount, Ollie faces a choice: whether to return to a way of life she thought she'd left behind for just one more score or to leave it all behind. A taught and affecting thriller about sisters pushed to extremes from award-winning director Nia DaCosta.",
                    age_restriction = "R"

                },
                new Movie
                {
                    title = "WOMAN AT WAR (KONA FER Í STRÍÐ)",
                    genre = "Action & Adventure",
                    poster_url = "https://resizing.flixster.com/5uw-2Jlqsn761CI5vnv4R1HtX-E=/fit-in/200x296.2962962962963/v1.bTsxMjk2NzEzMjtqOzE4MTUwOzEyMDA7Mjc2NDs0MDk2",
                    description = "Halla is a fifty-year-old independent woman. But behind the scenes of a quiet routine, she leads a double life as a passionate environmental activist. Known to others only by her alias 'The Woman of the Mountain,' Halla secretly wages a one-woman-war on the local aluminum industry. As Halla's actions grow bolder, from petty vandalism to outright industrial sabotage, she succeeds in pausing the negotiations between the Icelandic government and the corporation building a new aluminum smelter. But right as she begins planning her biggest and boldest operation yet, she receives an unexpected letter that changes everything. Her application to adopt a child has finally been accepted and there is a little girl waiting for her in Ukraine. As Halla prepares to abandon her role as saboteur and savior of the Highlands to fulfill her dream of becoming a mother, she decides to plot one final attack to deal the aluminum industry a crippling blow.",
                    age_restriction = "NR"

                },
                new Movie
                {
                    title = "BOOKSMART",
                    genre = "Comedy",
                    poster_url = "https://resizing.flixster.com/N1unAboDNL5CMzeFXSiX4ITRXJk=/fit-in/200x296.2962962962963/v1.bTsxMzAxNzc1MjtqOzE4MTUwOzEyMDA7MjAyNjszMDAx",
                    description = "The story follows Dever and Feldstein's characters, two academic superstars and best friends who, on the eve of their high school graduation, suddenly realize that they should have worked less and played more. Determined never to fall short of their peers, the girls set out on a mission to cram four years of fun into one night.",
                    age_restriction = "R"

                },
                new Movie
                {
                    title = "AVENGERS: ENDGAME",
                    genre = "Action & Adventure",
                    poster_url = "https://resizing.flixster.com/64AmNcAnJcG7k3u4oe_oJ0M-xm8=/fit-in/200x296.2962962962963/v1.bTsxMzAxOTkzMjtqOzE4MTUwOzEyMDA7MTY4ODsyNTAw",
                    description = "The grave course of events set in motion by Thanos that wiped out half the universe and fractured the Avengers ranks compels the remaining Avengers to take one final stand in Marvel Studios' grand conclusion to twenty-two films, 'Avengers: Endgame.'",
                    age_restriction = "PG-13"

                }};

                //Timeslots:
                var timeslots = new List<TimeSlot>{
                new TimeSlot
                {
                    time = "09:00",
                    duration =120 //in minutes
                },
                new TimeSlot
                {
                    time = "12:00",
                    duration =120 //in minutes
                },
                new TimeSlot
                {
                    time = "15:00",
                    duration =120 //in minutes
                },
                new TimeSlot
                {
                    time = "18:00",
                    duration =120 //in minutes
                },
                new TimeSlot
                {
                    time = "21:00",
                    duration =120 //in minutes
                }
                };


                // Cinemas:
                var cinemas = new List<Cinema>()
                {
                    new Cinema
                    {
                        capacity = 40
                    },
                    new Cinema
                    {
                        capacity = 60
                    },
                    new Cinema
                    {
                        capacity = 100
                    }
                };

                // Shows:
                Random rand = new Random();
                var shows = new List<Show>();

                // foreach ( day in dates)//create a show for every date
                // {
                    foreach (TimeSlot time in timeslots)//for every timeslot on that date
                    {
                        foreach (Cinema cin in cinemas)//in every cinema
                        {
                            shows.Add(
                                new Show()
                                {
                                    movie = movies[rand.Next(0,movies.Count()-1)],//insert random movie into show
                                    timeslot = time,
                                    date = 1563465921846,
                                    cinema = cin,
                                    bookings = null
                                }
                         );
                        }
                    }

                // };


               
                context.Movies.AddRange(movies);
                context.TimeSlots.AddRange(timeslots);
                context.Cinemas.AddRange(cinemas);
                context.Shows.AddRange(shows);
                context.SaveChanges();
            }
        }
    }//1563465921846
}