﻿// <auto-generated />
using System;
using BananaIncBooking.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace BananaIncBooking.Migrations
{
    [DbContext(typeof(BookingContext))]
    partial class BookingContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.0.0-preview5.19227.1");

            modelBuilder.Entity("BananaIncBooking.Models.Booking", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("seat");

                    b.Property<int?>("showid");

                    b.Property<string>("user");

                    b.HasKey("id");

                    b.HasIndex("showid");

                    b.ToTable("Bookings");
                });

            modelBuilder.Entity("BananaIncBooking.Models.Cinema", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("capacity");

                    b.HasKey("id");

                    b.ToTable("Cinemas");
                });

            modelBuilder.Entity("BananaIncBooking.Models.Movie", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("age_restriction");

                    b.Property<string>("description");

                    b.Property<string>("genre");

                    b.Property<string>("poster_url");

                    b.Property<string>("title");

                    b.HasKey("id");

                    b.ToTable("Movies");
                });

            modelBuilder.Entity("BananaIncBooking.Models.Show", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("cinemaid");

                    b.Property<long>("date");

                    b.Property<int?>("movieid");

                    b.Property<int?>("timeslotid");

                    b.HasKey("id");

                    b.HasIndex("cinemaid");

                    b.HasIndex("movieid");

                    b.HasIndex("timeslotid");

                    b.ToTable("Shows");
                });

            modelBuilder.Entity("BananaIncBooking.Models.TimeSlot", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("duration");

                    b.Property<string>("time");

                    b.HasKey("id");

                    b.ToTable("TimeSlots");
                });

            modelBuilder.Entity("BananaIncBooking.Models.Booking", b =>
                {
                    b.HasOne("BananaIncBooking.Models.Show", "show")
                        .WithMany("bookings")
                        .HasForeignKey("showid")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("BananaIncBooking.Models.Show", b =>
                {
                    b.HasOne("BananaIncBooking.Models.Cinema", "cinema")
                        .WithMany()
                        .HasForeignKey("cinemaid");

                    b.HasOne("BananaIncBooking.Models.Movie", "movie")
                        .WithMany()
                        .HasForeignKey("movieid");

                    b.HasOne("BananaIncBooking.Models.TimeSlot", "timeslot")
                        .WithMany()
                        .HasForeignKey("timeslotid");
                });
#pragma warning restore 612, 618
        }
    }
}
