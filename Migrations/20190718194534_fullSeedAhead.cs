﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace BananaIncBooking.Migrations
{
    public partial class fullSeedAhead : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cinemas",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    capacity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cinemas", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    title = table.Column<string>(nullable: true),
                    genre = table.Column<string>(nullable: true),
                    poster_url = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    age_restriction = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "TimeSlots",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    time = table.Column<string>(nullable: true),
                    duration = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimeSlots", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Shows",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    movieid = table.Column<int>(nullable: true),
                    timeslotid = table.Column<int>(nullable: true),
                    date = table.Column<long>(nullable: false),
                    cinemaid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shows", x => x.id);
                    table.ForeignKey(
                        name: "FK_Shows_Cinemas_cinemaid",
                        column: x => x.cinemaid,
                        principalTable: "Cinemas",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Shows_Movies_movieid",
                        column: x => x.movieid,
                        principalTable: "Movies",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Shows_TimeSlots_timeslotid",
                        column: x => x.timeslotid,
                        principalTable: "TimeSlots",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Bookings",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    seat = table.Column<int>(nullable: false),
                    user = table.Column<string>(nullable: true),
                    showid = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bookings", x => x.id);
                    table.ForeignKey(
                        name: "FK_Bookings_Shows_showid",
                        column: x => x.showid,
                        principalTable: "Shows",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Bookings_showid",
                table: "Bookings",
                column: "showid");

            migrationBuilder.CreateIndex(
                name: "IX_Shows_cinemaid",
                table: "Shows",
                column: "cinemaid");

            migrationBuilder.CreateIndex(
                name: "IX_Shows_movieid",
                table: "Shows",
                column: "movieid");

            migrationBuilder.CreateIndex(
                name: "IX_Shows_timeslotid",
                table: "Shows",
                column: "timeslotid");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Bookings");

            migrationBuilder.DropTable(
                name: "Shows");

            migrationBuilder.DropTable(
                name: "Cinemas");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "TimeSlots");
        }
    }
}
