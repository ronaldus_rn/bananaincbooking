using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using BananaIncBooking.Models;
using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.Extensions.Options;
// using System.ComponentModel.DataAnnotations;//for key atribbute

namespace BananaIncBooking.Models
{
    public class Show
    {
        public int id {get; set; }
        public Movie movie { get; set; }
        public TimeSlot timeslot { get; set; }
        public long date { get; set; }//milliseconds since UTC epoch
        public Cinema cinema { get; set; }
        public IEnumerable<Booking> bookings { get; set; }
        
    }         
}