using System.ComponentModel.DataAnnotations;

namespace BananaIncBooking.Models.BindingTargets
{
    public class TimeSlotData
    {
        public int Id { get => TimeSlot.id; set => TimeSlot.id = value; }
        [Required]
        public int Duration { get => TimeSlot.duration; set => TimeSlot.duration = value; }
        [Required]
        public string Time { get => TimeSlot.time; set => TimeSlot.time = value; }
        
        public TimeSlot TimeSlot { get; set; } = new TimeSlot();
    }
}