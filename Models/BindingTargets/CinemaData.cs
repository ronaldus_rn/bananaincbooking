using System.ComponentModel.DataAnnotations;

namespace BananaIncBooking.Models.BindingTargets
{
    public class CinemaData
    {
        public int Id { get => Cinema.id; set => Cinema.id = value; }
        // [Required]
        public int Capacity { get => Cinema.capacity; set => Cinema.capacity = value; }
        // [Required]
        
        public Cinema Cinema { get; set; } = new Cinema();
    }
}