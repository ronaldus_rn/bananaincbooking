using System.ComponentModel.DataAnnotations;

namespace BananaIncBooking.Models.BindingTargets
{
    public class MovieData
    {
        public int Id { get => Movie.id; set => Movie.id = value; }
        [Required]
        public string Title { get => Movie.title; set => Movie.title = value; }
        [Required]
        public string Genre { get => Movie.genre; set => Movie.genre = value; }
        [Required]
        public string PosterUrl { get => Movie.poster_url; set => Movie.poster_url = value; }
        [Required]
        public string Description { get => Movie.description; set => Movie.description = value; }
        public string AgeRestriction { get => Movie.age_restriction; set => Movie.age_restriction = value; }

        public Movie Movie { get; set; } = new Movie();
    }
}