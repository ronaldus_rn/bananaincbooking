using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace BananaIncBooking.Models.BindingTargets
{
    public class ShowData
    {
        public int Id { get => Show.id; set => Show.id = value; }
        public Movie Movie { get => Show.movie; set => Show.movie = value; }
        public long Date { get => Show.date; set => Show.date = value; }
        public TimeSlot TimeSlot { get => Show.timeslot; set => Show.timeslot = value; }
        public Cinema Cinema { get => Show.cinema; set => Show.cinema = value; }
        public IEnumerable<Booking> Bookings { get => Show.bookings; set => Show.bookings = value; }
                
        public Show Show { get; set; } = new Show();
    }
}
