using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace BananaIncBooking.Models.BindingTargets
{
    public class BookingData
    {
        public int Id { get => Booking.id; set => Booking.id = value; }
        public int Seat { get => Booking.seat; set => Booking.seat = value; }
        public string User { get => Booking.user; set => Booking.user = value; }
        public Show Show { get => Booking.show; set => Booking.show = value; }
        
        public Booking Booking { get; set; } = new Booking();
    }
}
