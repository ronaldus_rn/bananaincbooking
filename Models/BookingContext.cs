using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using BananaIncBooking.Models;
using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.Extensions.Options;
// using System.ComponentModel.DataAnnotations;//for key atribbute

namespace BananaIncBooking.Models
{
    public class BookingContext : DbContext
    {
        public BookingContext(DbContextOptions<BookingContext> options)
            : base(options)
        { }

        public DbSet<Movie> Movies { get; set; }
        public DbSet<Show> Shows { get; set; }
        public DbSet<TimeSlot> TimeSlots { get; set; }
        public DbSet<Cinema> Cinemas { get; set; }
        public DbSet<Booking> Bookings { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Show>().HasMany<Booking>(sh => sh.bookings).WithOne(b => b.show).OnDelete(DeleteBehavior.Cascade);
        }        
    }          
}