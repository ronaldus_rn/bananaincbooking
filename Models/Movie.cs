using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using BananaIncBooking.Models;
using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.Extensions.Options;
// using System.ComponentModel.DataAnnotations;//for key atribbute

namespace BananaIncBooking.Models
{
    public class Movie
    {
        public int id { get; set; }
        public string title { get; set; }
        public string genre { get; set; }
        public string poster_url { get; set; }
        public string description { get; set; }
        public string age_restriction { get; set; }

    }
             
}