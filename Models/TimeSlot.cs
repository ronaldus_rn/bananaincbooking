using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using BananaIncBooking.Models;
using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.Extensions.Options;
// using System.ComponentModel.DataAnnotations;//for key atribbute

namespace BananaIncBooking.Models
{

    public class TimeSlot
    {
        public int id {get; set; }
        public string time { get; set; }
        public int duration { get; set; } //in minutes
        
    }
}