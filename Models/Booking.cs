using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using BananaIncBooking.Models;
using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.Extensions.Options;
// using System.ComponentModel.DataAnnotations;//for key atribbute

namespace BananaIncBooking.Models
{
    public class Booking  
    {
        public int id {get; set; }
        public int seat { get; set; }
        public string user { get; set; }
        public Show show { get; set; }
    }             
}