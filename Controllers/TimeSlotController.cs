using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using BananaIncBooking.Models;
using BananaIncBooking.Models.BindingTargets;
using Microsoft.AspNetCore.JsonPatch;

namespace BananaIncBooking.Controllers
{
    [Route("api/timeslots")]
    public class TimeSlotController : Controller
    {
        private BookingContext context;
        public TimeSlotController(BookingContext ctx){
            context = ctx;
        }


        [HttpGet("{id}")]
        public TimeSlot GetTimeSlot(int id)
        {
            return context.TimeSlots.Find(id);
        }

        [HttpGet]
        public IEnumerable<TimeSlot> GetTimeSlots()
        {
            IQueryable<TimeSlot> rtTimeSlot = context.TimeSlots;

            return rtTimeSlot;            
        }

        [HttpPost]
        public IActionResult CreateTimeSlot([FromBody] TimeSlotData tdata)
        {
            if (ModelState.IsValid)
            {
                TimeSlot t = tdata.TimeSlot;
                context.Add(t);
                context.SaveChanges();
                return Ok(t.id);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPut("{id}")]
        public IActionResult ReplaceTimeSlot(int id, 
                [FromBody] TimeSlotData tdata)
        {
            if (ModelState.IsValid)
            {
                TimeSlot t = tdata.TimeSlot;
                t.id = id;
                context.Update(t);
                context.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPatch("{id}")]
        public IActionResult UpdateTimeSlot(int id,
            [FromBody]JsonPatchDocument<TimeSlotData> patch)
        {
            TimeSlot timeSlot = context.TimeSlots.First(t => t.id == id);
            TimeSlotData tdata= new TimeSlotData { TimeSlot = timeSlot };
            patch.ApplyTo(tdata, ModelState);
            if (ModelState.IsValid && TryValidateModel(tdata))
            {
                context.SaveChanges();
                return Ok(timeSlot);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }


        [HttpDelete("{id}")]
        public IActionResult  DeleteTimeSlot(int id)
        {
            context.TimeSlots.Remove(new TimeSlot { id = id });
            context.SaveChanges();
            return Ok(id);
        }         

    }
}
