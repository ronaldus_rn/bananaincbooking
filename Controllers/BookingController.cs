using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using BananaIncBooking.Models;
using BananaIncBooking.Models.BindingTargets;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;

namespace BananaIncBooking.Controllers
{
    [Route("api/bookings")]
public class BookingController : Controller
    {
        private BookingContext context;
        public BookingController(BookingContext ctx){
            context = ctx;
        }


        [HttpGet("{id}")]
        public Booking GetBooking(int id)
        {
            Booking rtBooking = context.Bookings
                .Include(b => b.show)
                    .ThenInclude(s => s.movie)
                .Include(b => b.show)
                    .ThenInclude(s => s.timeslot)
                // .Include(b => b.show)
                //     .ThenInclude(s => s.date)
                .Include(b => b.show)
                    .ThenInclude(s => s.cinema)
                .FirstOrDefault(b => b.id == id);
            if (rtBooking != null)
            {
                if (rtBooking.show != null)
                {
                    rtBooking.show = new Show{
                        id = rtBooking.show.id,
                        movie = rtBooking.show.movie,
                        timeslot = rtBooking.show.timeslot,
                        date = rtBooking.show.date,
                        cinema = rtBooking.show.cinema
                    };
                    
                }
                
            }
            return rtBooking;
        }

        // [HttpGet]
        // public IEnumerable<Booking> GetBookings(bool related = true)
        // {
        //     IQueryable<Booking> rtBooking = context.Bookings;
        //     if (related)
        //     {
        //         rtBooking = rtBooking
        //             .Include(b => b.show);
        //     }
        //     return rtBooking;            
        // }

        [HttpGet]
        public IActionResult GetBookings(string user, bool related = true, bool metadata = false )
        {
            IQueryable<Booking> rtBookings = context.Bookings;

            if (!string.IsNullOrWhiteSpace(user))
            {
                string userLower = user.ToLower();
                rtBookings = rtBookings.Where(b => b.user.ToLower().Contains(userLower));
            }

            if (related)
            {
                rtBookings = rtBookings
                    .Include(b => b.show)
                        .ThenInclude(s => s.movie)
                    .Include(b => b.show)
                        .ThenInclude(s => s.timeslot)
                    // .Include(b => b.show)
                    //     .ThenInclude(s => s.date)
                    .Include(b => b.show)
                        .ThenInclude(s => s.cinema);
                List<Booking> bookingsListRelated = rtBookings.ToList();
                bookingsListRelated.ForEach ( bk =>
                {
                    if (bk.show != null)
                    {
                        bk.show = new Show{
                        id = bk.show.id,
                        movie = bk.show.movie,
                        timeslot = bk.show.timeslot,
                        date = bk.show.date,
                        cinema = bk.show.cinema
                    };
                    }

                });
                return metadata ? CreateMetadata(bookingsListRelated) : Ok(bookingsListRelated);
            }
            else
            {
                return metadata ? CreateMetadata(rtBookings) : Ok(rtBookings);            
            }
        }
//metadata to generate list of users from db
         private IActionResult CreateMetadata(IEnumerable<Booking> bookings)
        {
            return Ok(new
            {
                data = bookings,
                users = context.Bookings.Select(b => b.user)
            .Distinct().OrderBy(b => b)
            });
        }


        [HttpPost]
        public IActionResult CreateBooking([FromBody] BookingData bdata)
        {
            if (ModelState.IsValid)
            {
                Booking b = bdata.Booking;
                if (b.show != null && b.show.id != 0)
                {
                    context.Attach(b.show);
                }                
                context.Add(b);
                context.SaveChanges();
                return Ok(b.id);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPut("{id}")]
        public IActionResult ReplaceBooking(int id, [FromBody] BookingData bdata)
        {
            if (ModelState.IsValid)
            {
                Booking b = bdata.Booking;
                b.id = id;
                context.Update(b);
                context.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPatch("{id}")]
        public IActionResult UpdateBooking(int id,
            [FromBody]JsonPatchDocument<BookingData> patch)
        {
            Booking booking = context.Bookings.First(b => b.id == id);
            BookingData bdata= new BookingData { Booking = booking };
            patch.ApplyTo(bdata, ModelState);
            if (ModelState.IsValid && TryValidateModel(bdata))
            {
                context.SaveChanges();
                return Ok(booking);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }


        [HttpDelete("{id}")]
        public IActionResult  DeleteBooking(int id)
        {
            context.Bookings.Remove(new Booking { id = id });
            context.SaveChanges();
            return Ok(id);
        }         
          
    }
}