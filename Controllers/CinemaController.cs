using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using BananaIncBooking.Models;
using BananaIncBooking.Models.BindingTargets;
using Microsoft.AspNetCore.JsonPatch;

namespace BananaIncBooking.Controllers
{
    [Route("api/cinemas")]
    public class CinemaController : Controller
    {
        private BookingContext context;
        public CinemaController(BookingContext ctx){
            context = ctx;
        }


        [HttpGet("{id}")]
        public Cinema GetCinema(int id)
        {
            return context.Cinemas.Find(id);
        }

        [HttpGet]
        public IEnumerable<Cinema> GetCinemas()
        {
            IQueryable<Cinema> rtCinema = context.Cinemas;

            return rtCinema;            
        }

        [HttpPost]
        public IActionResult CreateCinema([FromBody] CinemaData cdata)
        {
            if (ModelState.IsValid)
            {
                Cinema c = cdata.Cinema;
                context.Add(c);
                context.SaveChanges();
                return Ok(c.id);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPut("{id}")]
        public IActionResult ReplaceCinema(int id, 
            [FromBody] CinemaData cdata)
        {
            if (ModelState.IsValid)
            {
                Cinema c = cdata.Cinema;
                c.id = id;
                context.Update(c);
                context.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPatch("{id}")]
        public IActionResult UpdateCinema(int id,
            [FromBody]JsonPatchDocument<CinemaData> patch)
        {
            Cinema cinema = context.Cinemas.First(m => m.id == id);
            CinemaData cdata= new CinemaData { Cinema = cinema };
            patch.ApplyTo(cdata, ModelState);
            if (ModelState.IsValid && TryValidateModel(cdata))
            {
                context.SaveChanges();
                return Ok(cinema);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }


        [HttpDelete("{id}")]
        public IActionResult  DeleteCinema(int id)
        {
            context.Cinemas.Remove(new Cinema { id = id });
            context.SaveChanges();
            return Ok(id);
        }         

    }
}
