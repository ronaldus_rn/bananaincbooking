using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using BananaIncBooking.Models;
using BananaIncBooking.Models.BindingTargets;
using Microsoft.AspNetCore.JsonPatch;

namespace BananaIncBooking.Controllers
{
    [Route("api/movies")]
    public class MovieController : Controller
    {
        private BookingContext context;
        public MovieController(BookingContext ctx){
            context = ctx;
        }

        [HttpGet("{id}")]
        public Movie GetMovie(int id)
        {
            return context.Movies.Find(id);
        }

        [HttpGet]
        public IActionResult GetMovies(string genre, string search, bool metadata = false )
        {
            IQueryable<Movie> query = context.Movies;

            if (!string.IsNullOrWhiteSpace(genre))
            {
                string genreLower = genre.ToLower();
                query = query.Where(m => m.genre.ToLower().Contains(genreLower));
            }
            if (!string.IsNullOrWhiteSpace(search))
            {
                string searchLower = search.ToLower();
                query = query.Where(m => m.title.ToLower().Contains(searchLower)
                || m.description.ToLower().Contains(searchLower));
            }

            return metadata ? CreateMetadata(query) : Ok(query);            
        }
//metadata to generate list of genres from db
         private IActionResult CreateMetadata(IEnumerable<Movie> movies)
        {
            return Ok(new
            {
                data = movies,
                genres = context.Movies.Select(m => m.genre)
                    .Distinct().OrderBy(m => m)
            });
        }

        [HttpPost]
        public IActionResult CreateMovie([FromBody] MovieData mdata)
        {
            if (ModelState.IsValid)
            {
                Movie m = mdata.Movie;
                context.Add(m);
                context.SaveChanges();
                return Ok(m.id);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPut("{id}")]
        public IActionResult ReplaceMovie(int id, [FromBody] MovieData mData)
        {
            if (ModelState.IsValid)
            {
                Movie m = mData.Movie;
                m.id = id;
                context.Update(m);
                context.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPatch("{id}")]
        public IActionResult UpdateMovie(int id,
            [FromBody]JsonPatchDocument<MovieData> patch)
        {
            Movie movie = context.Movies.First(m => m.id == id);
            MovieData mdata = new MovieData { Movie = movie };
            patch.ApplyTo(mdata, ModelState);
            if (ModelState.IsValid && TryValidateModel(mdata))
            {
                context.SaveChanges();
                return Ok(movie);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }


        [HttpDelete("{id}")]
        public IActionResult  DeleteMovie(int id)
        {
            context.Movies.Remove(new Movie { id = id });
            context.SaveChanges();
            return Ok(id);
        }                      

    }
}