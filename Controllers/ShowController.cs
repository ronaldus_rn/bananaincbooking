using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using BananaIncBooking.Models;
using BananaIncBooking.Models.BindingTargets;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.EntityFrameworkCore;

namespace BananaIncBooking.Controllers
{
    [Route("api/shows")]
    public class ShowController : Controller
    {
        private BookingContext context;
        public ShowController(BookingContext ctx)
        {
            context = ctx;
        }


        [HttpGet("{id}")]
        public Show GetShow(int id)
        {
            Show rtShow = context.Shows
                .Include(sh => sh.movie)
                .Include(sh => sh.timeslot)
                // .Include(sh => sh.date)
                .Include(sh => sh.cinema)
                .Include(sh => sh.bookings).ThenInclude(b => b.show)
                .FirstOrDefault(sh => sh.id == id);
            if (rtShow != null)
            {
                if (rtShow.bookings != null)
                {
                    rtShow.bookings = rtShow.bookings.Select(b =>
                    new Booking
                    {
                        id = b.id,
                        seat = b.seat,
                        user = b.user
                    });
                }
            }
            return rtShow;
        }

        // [HttpGet]
        // public IActionResult GetShows(string movie, long start_date = 0, long end_date = 0,int cinema = 0 , string time = "", bool metadata = false, bool related = true, bool include_null_movies = false)
        // {
        //     IQueryable<Show> rtShows = context.Shows;

        //     if (!include_null_movies)
        //     {
        //         rtShows = rtShows.Where(s => s.movie != null);
        //     }

        //     if (!string.IsNullOrWhiteSpace(movie))
        //     {
        //         string movieLower = movie.ToLower();
        //         rtShows = rtShows.Where(s => s.movie.title.ToLower().Contains(movieLower));
        //     }
        //     if (start_date != 0 && end_date != 0)
        //     {
        //         rtShows = rtShows.Where(s => (s.date >= start_date && s.date <= end_date));
        //     }
        //     else
        //     {
        //         if (start_date != 0)
        //         {
        //             rtShows = rtShows.Where(s => (s.date >= start_date));
        //         }
        //         if (end_date != 0)
        //         {
        //             rtShows = rtShows.Where(s => (s.date <= end_date));
        //         }
        //     }

        //     if (cinema != 0)//will work with cinema ID
        //     {
        //         rtShows = rtShows.Where(s => (s.cinema.id == cinema));
        //     }            

        //     if (!string.IsNullOrWhiteSpace(time))
        //     {
        //         string timeLower = time.ToLower();
        //         rtShows = rtShows.Where(s => s.timeslot.time.ToLower().Contains(timeLower));
        //     }
        //     if (related)
        //     {
        //         rtShows = rtShows
        //             .Include(sh => sh.movie)
        //             .Include(sh => sh.timeslot)
        //             .Include(sh => sh.cinema)
        //             .Include(sh => sh.bookings);
        //         List<Show> showListRelated = rtShows.ToList();
        //         showListRelated.ForEach(sh =>
        //        {
        //            if (sh.bookings != null)
        //            {
        //                sh.bookings.ToList().ForEach(b => b.show = null);
        //            }
        //        });
        //         return metadata ? CreateMetadata(showListRelated) : Ok(showListRelated);
        //     }
        //     else
        //     {
        //         return metadata ? CreateMetadata(rtShows) : Ok(rtShows);
        //     }
        // 
        // }

        [HttpGet]
        public IActionResult GetShows(string movie, long start_date = 0, long end_date = 0, int cinema = 0, string time = "", bool metadata = false, bool admin = false)
        {
            if (admin)
            {
                {
                    IQueryable<Show> rtShows = context.Shows;

                    if (start_date != 0 && end_date != 0)
                    {
                        rtShows = rtShows.Where(s => (s.date >= start_date && s.date <= end_date));
                    }
                    else
                    {
                        if (start_date != 0)
                        {
                            rtShows = rtShows.Where(s => (s.date >= start_date));
                        }
                        if (end_date != 0)
                        {
                            rtShows = rtShows.Where(s => (s.date <= end_date));
                        }
                    }

                    if (cinema != 0)//will work with cinema ID
                    {
                        rtShows = rtShows.Where(s => (s.cinema.id == cinema));
                    }

                    if (!string.IsNullOrWhiteSpace(movie))
                    {
                        string movieLower = movie.ToLower();
                        rtShows = rtShows.Where(s => s.movie.title.ToLower().Contains(movieLower));
                    }
                    if (!string.IsNullOrWhiteSpace(time))
                    {
                        string timeLower = time.ToLower();
                        rtShows = rtShows.Where(s => s.timeslot.time.ToLower().Contains(timeLower));
                    }

                    rtShows = rtShows
                        .Include(sh => sh.movie)
                        .Include(sh => sh.timeslot)
                        .Include(sh => sh.cinema)
                        .Include(sh => sh.bookings);
                    List<Show> showListRelated = rtShows.ToList();
                    showListRelated.ForEach(sh =>
                   {
                       if (sh.bookings != null)
                       {
                           sh.bookings.ToList().ForEach(b => b.show = null);
                       }
                   });
                    return metadata ? CreateAdminMetadata(showListRelated) : Ok(showListRelated);
                }
            }
            else
            {

                IQueryable<Show> rtShows = context.Shows;
                rtShows = rtShows.Where(s => s.movie != null);//do not include shows that have no movies

                if (!string.IsNullOrWhiteSpace(movie))
                {
                    string movieLower = movie.ToLower();
                    rtShows = rtShows.Where(s => s.movie.title.ToLower().Contains(movieLower));
                }
                if (start_date != 0 && end_date != 0)
                {
                    rtShows = rtShows.Where(s => (s.date >= start_date && s.date <= end_date));
                }
                else
                {
                    if (start_date != 0)
                    {
                        rtShows = rtShows.Where(s => (s.date >= start_date));
                    }
                    if (end_date != 0)
                    {
                        rtShows = rtShows.Where(s => (s.date <= end_date));
                    }
                }

                if (cinema != 0)//will work with cinema ID
                {
                    rtShows = rtShows.Where(s => (s.cinema.id == cinema));
                }

                if (!string.IsNullOrWhiteSpace(time))
                {
                    string timeLower = time.ToLower();
                    rtShows = rtShows.Where(s => s.timeslot.time.ToLower().Contains(timeLower));
                }
                rtShows = rtShows
                    .Include(sh => sh.movie)
                    .Include(sh => sh.timeslot)
                    .Include(sh => sh.cinema)
                    .Include(sh => sh.bookings);
                List<Show> showListRelated = rtShows.ToList();
                showListRelated.ForEach(sh =>
               {
                   if (sh.bookings != null)
                   {
                       sh.bookings.ToList().ForEach(b => b.show = null);
                   }
               });
                return metadata ? CreateMetadata(showListRelated) : Ok(showListRelated);
            }

        }



        //metadata to generate list of movies,dates and times in available shows from db
        private IActionResult CreateMetadata(IEnumerable<Show> shows)
        {
            return Ok(new
            {
                data = shows,
                movies = context.Shows.Select(s => s.movie.title)
                    .Distinct().OrderBy(t => t),
                dates = shows.Select(s => s.date)//dates and times are selected from shows, instead of context.shows
                    .Distinct().OrderBy(y => y),//in order to generate list of only dates and times that contain specified show/movie
                times = shows.Select(s => s.timeslot.time)
                    .Distinct().OrderBy(t => t)
            });
        }
        private IActionResult CreateAdminMetadata(IEnumerable<Show> shows)
        {
            return Ok(new
            {
                data = shows,
                movies = context.Shows.Select(s => s.movie.title)
                    .Distinct().OrderBy(t => t),
                dates = context.Shows.Select(s => s.date)
                    .Distinct().OrderBy(y => y),
                cinemas = context.Shows.Select(s => s.cinema.id)
                    .Distinct().OrderBy(c => c),
                times = context.Shows.Select(s => s.timeslot.time)
                    .Distinct().OrderBy(t => t)
            });
        }

        [HttpPost]
        public IActionResult CreateShow([FromBody] ShowData sdata)
        {
            if (ModelState.IsValid)
            {
                Show s = sdata.Show;
                if (s.movie != null && s.movie.id != 0)
                {
                    context.Attach(s.movie);
                }
                if (s.timeslot != null && s.timeslot.id != 0)
                {
                    context.Attach(s.timeslot);
                }
                // if (s.date != null && s.date.id != 0)
                // {
                //     context.Attach(s.date);
                // }                
                if (s.cinema != null && s.cinema.id != 0)
                {
                    context.Attach(s.cinema);
                }
                // if (s.bookings != null && s.bookings.id != 0)
                // {
                //     context.Attach(s.bookings);
                // }                
                context.Add(s);
                context.SaveChanges();
                return Ok(s.id);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPut("{id}")]
        public IActionResult ReplaceShow(int id, [FromBody] ShowData sdata)
        {
            if (ModelState.IsValid)
            {
                Show s = sdata.Show;
                s.id = id;
                context.Update(s);
                context.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpPatch("{id}")]
        public IActionResult UpdateShow(int id,
            [FromBody]JsonPatchDocument<ShowData> patch)
        {
            Show show = context.Shows.First(sh => sh.id == id);
            ShowData sdata = new ShowData { Show = show };
            patch.ApplyTo(sdata, ModelState);
            if (ModelState.IsValid && TryValidateModel(sdata))
            {
                context.SaveChanges();
                return Ok(show);
            }
            else
            {
                return BadRequest(ModelState);
            }
        }


        [HttpDelete("{id}")]
        public IActionResult DeleteShow(int id)
        {
            context.Shows.Remove(new Show { id = id });
            context.SaveChanges();
            return Ok(id);
        }

    }
}