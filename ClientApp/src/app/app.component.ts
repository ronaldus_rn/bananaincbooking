import { Component } from '@angular/core';
import { MovieRepository } from "./Repositories/movie.repository";
import { Movie } from "./models/movie.model";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private movie_repo: MovieRepository){
  }
  ngOnInit() {
    this.movie_repo.getMovies();
    // this.movie_repo.getCinemas();
    // this.movie_repo.getTimeSlots();
    this.movie_repo.getShows();
  }
}
