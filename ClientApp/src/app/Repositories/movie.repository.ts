import { HttpClient } from '@angular/common/http'; // added
import { Inject, Injectable } from '@angular/core'; // added
import { Observable, throwError } from 'rxjs';
// import { Cinema } from "../models/cinema.model";
// import { TimeSlot } from "../models/timeslot.model";
import { Movie } from "../models/movie.model";
import { Show } from "../models/show.model";
import { Cinema } from "../models/cinema.model";
import { TimeSlot } from "../models/timeslot.model";
import { Booking } from '../models/booking.model';

import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import * as _moment from 'moment';


const moment = _moment;

const moviesUrl = "api/movies";
const cinemasUrl = "api/cinemas";
const showsUrl = "api/shows";
const timeSlotUrl = "api/timeslots"
const bookingsUrl = "api/bookings"


@Injectable()
export class MovieRepository {

	private movieFilterObject = new movieFilter();
	private bookingFilterObject = new bookingFilter();
	private showFilterObject = new showFilter();
	private showFilterObject_day = new showFilter();//used for filtering days when making a booking

	constructor(private http: HttpClient) {
	}

	// Movie functions and vars: **************************************************************************
	public movie: Movie;
	public movies: Movie[];
	public genres: string[] = [];

	getMovie(id: number) {
		this.http.get(moviesUrl + "/" + id)
			.subscribe(response => { this.movie = response });
	}

	getMovies() {
		let url = moviesUrl;
		url += "?"
		if (this.moviefilter.genre) {
			url += "&genre=" + this.moviefilter.genre;
		}
		if (this.moviefilter.search) {
			url += "&search=" + this.moviefilter.search;
		}

		url += "&metadata=true";
		this.http.get<any>(url)
			.subscribe(response => {
				this.movies = response.data;
				this.genres = response.genres;
				// this.pagination.currentPage = 1;
			});
	}
	createMovie(mov: Movie) {
		let data = {
			Title: mov.title,
			Genre: mov.genre,
			PosterUrl: mov.poster_url,
			Description: mov.description,
			AgeRestriction: mov.age_restriction
		};
		this.http.post<number>(moviesUrl, data)
			.subscribe(response => {
				mov.id = response;
				this.movies.push(mov);
			});
	}

	replaceMovie(mov: Movie) {
		let data = {
			Title: mov.title,
			Genre: mov.genre,
			PosterUrl: mov.poster_url,
			Description: mov.description,
			AgeRestriction: mov.age_restriction
		};
		this.http.put(moviesUrl + "/" + mov.id, data)
			.subscribe(response => this.getMovies());
	}

	updateMovie(id: number, changes: Map<string, any>) {
		let patch = [];
		changes.forEach((value, key) =>
			patch.push({ op: "replace", path: key, value: value }));
		this.http.patch(moviesUrl + "/" + id, patch)
			.subscribe(response => this.getMovies());
	}

	deleteMovie(id: number) {
		this.http.delete(moviesUrl + "/" + id)
			.subscribe(response => this.getMovies());
	}


	get moviefilter(): movieFilter {
		return this.movieFilterObject;
	}

	// cinema functions  and vars****************************************************************************************************
	public cinema: Cinema;
	public cinemas: Cinema[];

	getCinema(id: number) {

		this.http.get(cinemasUrl + "/" + id)
			.subscribe(response => { this.cinema = response });
	}

	getCinemas() {
		let url = cinemasUrl;
		this.http.get<any>(url).subscribe(response => { this.cinemas = response });
	}

	createCinema(cin: Cinema) {
		let data = {
			Capacity: cin.capacity
		};
		this.http.post<number>(cinemasUrl, data)
			.subscribe(response => {
				cin.id = response;
				this.cinemas.push(cin);
			});
	}

	replaceCinema(cin: Cinema) {
		let data = {
			Capacity: cin.capacity,
		};
		this.http.put(cinemasUrl + "/" + cin.id, data)
			.subscribe(response => this.getCinemas());
	}

	deleteCinema(id: number) {
		this.http.delete(cinemasUrl + "/" + id)
			.subscribe(response => this.getCinemas());
	}



	// timeslot functions  and vars****************************************************************************************************
	public timeslot: TimeSlot;
	public timeslots: TimeSlot[];

	getTimeSlot(id: number) {

		this.http.get(timeSlotUrl + "/" + id)
			.subscribe(response => { this.timeslot = response });
	}

	getTimeSlots() {
		let url = timeSlotUrl;
		this.http.get<any>(url).subscribe(response => { this.timeslots = response });
	}

	createTimeSlot(tim: TimeSlot) {
		let data = {
			Time: tim.time,
			Duration: tim.duration
		};
		this.http.post<number>(timeSlotUrl, data)
			.subscribe(response => {
				tim.id = response;
				this.timeslots.push(tim);
			});
	}

	replaceTimeSlot(tim: TimeSlot) {
		let data = {
			Time: tim.time,
			Duration: tim.duration
		};
		this.http.put(timeSlotUrl + "/" + tim.id, data)
			.subscribe(response => this.getTimeSlots());
	}



	deleteTimeSlot(id: number) {
		this.http.delete(timeSlotUrl + "/" + id)
			.subscribe(response => this.getTimeSlots());
	}


	// show functions  and vars****************************************************************************************************
	public show: Show;
	public shows: Show[];
	public movies_str: string[] = [];
	public cinemas_str: string[] = [];
	// public years_num: number[] = [];
	// public months_num: number[] = [];
	// public days_num: number[] = [];
	public dates_moment: _moment.Moment[] = [];
	// public date: Date;
	public date_moment: _moment.Moment;
	// public moment_today: _moment.Moment;
	public times_str: string[] = [];

	getShow(id: number) {

		this.http.get(showsUrl + "/" + id)
			.subscribe(response => { this.show = response });
	}

	getShows(admin: Boolean = false) {
		let url = showsUrl + "?";
		if (this.showfilter.movie) {
			url += "&movie=" + this.showfilter.movie;
		}
		if (this.showfilter.start_date != null) {
			url += "&start_date=" + this.showfilter.start_date;
		}
		if (this.showfilter.end_date != null) {
			url += "&end_date=" + this.showfilter.end_date;
		}

		if (admin)
		{
			url += "&admin=true"
		}
		url += "&metadata=true";
		this.http.get<any>(url).subscribe(response => {
			this.shows = response.data,
				this.movies_str = response.movies,
				this.dates_moment = response.dates,
				this.times_str = response.times,
				this.cinemas_str = response.cinemas
		});

	}

	getShowsOnDay(admin: Boolean = false) {
		let url = showsUrl + "?";
		if (this.showfilter.movie) {
			url += "&movie=" + this.showfilter.movie;
		}
		if (this.showfilter_day.cinema) {
			url += "&cinema=" + this.showfilter_day.cinema;
		}

		if (this.showfilter_day.start_date != null) {
			url += "&start_date=" + this.showfilter_day.start_date;
		}
		if (this.showfilter_day.end_date != null) {
			url += "&end_date=" + this.showfilter_day.end_date;
		}
		if (admin)
		{
			url += "&admin=true"
		}		

		url += "&metadata=true";
		this.http.get<any>(url).subscribe(response => {
			this.shows = response.data
		});
	}



	createShow(sh: Show) {
		let data = {
			Movie: sh.movie,
			TimeSlot: sh.timeslot,
			Date: sh.date,
			Cinema: sh.cinema,
			Bookings: null
		};
		this.http.post<number>(showsUrl, data)
			.subscribe(response => {
				sh.id = response;
				this.shows.push(sh);
			});
	}

	replaceShow(sh: Show) {
		let data = {
			Movie: sh.movie,
			TimeSlot: sh.timeslot,
			Date: sh.date,
			Cinema: sh.cinema,
			Bookings: sh.bookings
		};
		this.http.put(showsUrl + "/" + sh.id, data)
			.subscribe(response => this.getShowsOnDay(true));
	}



	deleteShow(id: number) {//only admin can delete shows
		this.http.delete(showsUrl + "/" + id)
			.subscribe(response => this.getShows(true));
	}


	get showfilter(): showFilter {
		return this.showFilterObject;
	}
	get showfilter_day(): showFilter {
		return this.showFilterObject_day;
	}



	// Booking functions  and vars****************************************************************************************************
	public booking: Booking;
	public bookings: Booking[];
	public users_str: string[] = [];

	getBooking(id: number) {

		this.http.get(bookingsUrl + "/" + id)
			.subscribe(response => { this.booking = response });
	}

	getBookings() {
		let url = bookingsUrl;
		url += "?"
		if (this.bookingfilter.user) {
			url += "&user=" + this.bookingfilter.user;
		}
		url += "&metadata=true";
		this.http.get<any>(url)
			.subscribe(response => {
				this.bookings = response.data;
				this.users_str = response.users;
			});
	}

	createBooking(bk: Booking, usr: string) {
		let data = {
			Seat: bk.seat,
			User: usr,
			Show: bk.show
		};
		bk.user = usr;
		this.http.post<number>(bookingsUrl, data)
			.subscribe(response => {
				bk.id = response;
				this.bookings.push(bk);
			});
	}

	replaceBooking(bk: Booking) {
		let data = {
			Seat: bk.seat,
			User: bk.user,
			Show: bk.show
		};
		this.http.put(bookingsUrl + "/" + bk.id, data)
			.subscribe(response => this.getBookings());
	}

	deleteBooking(id: number) {
		this.http.delete(bookingsUrl + "/" + id)
			.subscribe(response => this.getBookings());
	}

	get bookingfilter(): bookingFilter {
		return this.bookingFilterObject;
	}

}

export class bookingFilter {
	user?: string;
	reset() {
		this.user = null;
	}
}
export class showFilter {
	movie?: string;
	time?: string;
	start_date?: number;
	end_date?: number;
	cinema?: number;//cinemas will be filltered by cinema ID
	reset() {
		this.movie = this.time = this.cinema = this.start_date = this.end_date = null;
	}
}

export class movieFilter {
	genre?: string;
	search?: string;
	reset() {
		this.genre = this.search = null;
	}
}
