import { Movie } from "./movie.model";
import { TimeSlot } from "./timeslot.model";
import { Cinema } from "./cinema.model";
import { Booking } from "./booking.model";


export class Show {
	constructor(
    	public id?: number,
    	public movie?: Movie,
    	public timeslot?: TimeSlot,
    	public date?: number,
    	public cinema?: Cinema,
    	public bookings?: Booking[]) { }
}