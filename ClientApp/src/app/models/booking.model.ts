import { Show } from "./show.model";

export class Booking {
	constructor(
    	public id?: number,
    	public seat?: number,
    	public user?: string,
    	public show?: Show) { }
}


