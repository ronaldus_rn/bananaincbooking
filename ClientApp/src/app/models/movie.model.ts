export class Movie {
	constructor(
		public id?: number,
		public title?: string,
		public genre?: string,
		public poster_url?: string,
		public description?: string,
		public age_restriction?: string
) { }
}

