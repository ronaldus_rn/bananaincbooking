import { NgModule } from "@angular/core";
import { MovieRepository } from "../Repositories/movie.repository";
 @NgModule({
	providers: [MovieRepository]
})
export class ModelModule { }