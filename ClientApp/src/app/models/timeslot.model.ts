export class TimeSlot {
	constructor(
    	public id?: number,
    	public time?: string,
    	public duration?: number) { }
}