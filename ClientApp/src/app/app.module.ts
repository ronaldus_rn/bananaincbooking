import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AuthorizeGuard } from 'src/api-authorization/authorize.guard';
import { AuthorizeInterceptor } from 'src/api-authorization/authorize.interceptor';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './components/nav-menu/nav-menu.component';
import { HomeComponent } from './components/home/home.component';
import { ListMoviesComponent } from './components/list-movies/list-movies.component'

import { ModelModule } from "./models/model.module";
import { AdminModule } from "./components/admin/admin.module"
import { ApiAuthorizationModule } from 'src/api-authorization/api-authorization.module';
import { AppRoutingModule } from './app-routing.module';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ListBookingsComponent } from './components/list-bookings/list-bookings.component';
import { ListShowsComponent } from './components/list-shows/list-shows.component';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
// import { DemoMaterialModule } from './material-module';
// import {MatNativeDateModule} from '@angular/material/core';


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    ListMoviesComponent,
    PageNotFoundComponent,
    ListBookingsComponent,
    ListShowsComponent,
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    ModelModule,
    FormsModule,
    // BrowserAnimationsModule,
    // FormsModule,
    // HttpClientModule,
    // DemoMaterialModule,
    // MatNativeDateModule,
    // ReactiveFormsModule,    
    ApiAuthorizationModule,
    AdminModule,
    AppRoutingModule
  ],
  // entryComponents: [HomeComponent],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthorizeInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
