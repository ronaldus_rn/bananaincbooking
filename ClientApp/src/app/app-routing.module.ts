import { NgModule } from '@angular/core';
import { AuthorizeGuard } from 'src/api-authorization/authorize.guard';
import { ListMoviesComponent } from './components/list-movies/list-movies.component';
import { HomeComponent } from './components/home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component'
import { RouterModule, Routes } from '@angular/router';
import { ListBookingsComponent } from './components/list-bookings/list-bookings.component';
import { ListShowsComponent } from './components/list-shows/list-shows.component';

const routes: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'list_movies', component: ListMoviesComponent, canActivate: [AuthorizeGuard] },
  { path: 'list_shows', component: ListShowsComponent, canActivate: [AuthorizeGuard] },
  { path: 'list_bookings', component: ListBookingsComponent, canActivate: [AuthorizeGuard] },
  { path: '', component: HomeComponent, pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
imports: [ RouterModule.forRoot(
    routes,
    {
      // enableTracing: false, // <-- debugging purposes only
      // preloadingStrategy: SelectivePreloadingStrategyService,
    }
  ) ],
exports: [ RouterModule ]
})
export class AppRoutingModule { }
