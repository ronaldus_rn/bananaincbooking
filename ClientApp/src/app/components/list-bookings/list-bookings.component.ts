import { Component, OnInit } from '@angular/core';
import { MovieRepository } from 'src/app/Repositories/movie.repository';
import { AuthorizeService } from 'src/api-authorization/authorize.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Booking } from 'src/app/models/booking.model';
import { Show } from 'src/app/models/show.model';
@Component({
  selector: 'app-list-bookings',
  templateUrl: './list-bookings.component.html',
  styleUrls: ['./list-bookings.component.css']
})
export class ListBookingsComponent implements OnInit {

  constructor(private repo: MovieRepository, private authorizeService: AuthorizeService) { 
  }

  public isAuthenticated: Observable<boolean>;
  public active_user: string;

  tableMode: boolean = true;


  selectBooking(id: number) {
    this.repo.getBooking(id);
  }
  saveBooking() {
    if (this.repo.booking.id == null) {
      this.repo.createBooking(this.repo.booking,this.active_user);
    } else {
      this.repo.replaceBooking(this.repo.booking);
    }
    this.clearBooking()
    this.tableMode = true;

  }
  deleteBooking(id: number) {
    this.repo.deleteBooking(id);
  }
  clearBooking() {
    this.repo.booking = new Booking();
    this.tableMode = true;
  }

  get booking(): Booking {
    return this.repo.booking;
  }
  get bookings(): Booking[] {
    return this.repo.bookings;
  }

  get users(): string[] {
    return this.repo.users_str
  }

  get currentUser(): string {
    return this.repo.bookingfilter.user;
  }
  setCurrentUser(newUser: string){
    this.repo.bookingfilter.user = newUser;
    this.repo.getBookings();
  }



  get show(): Show {
    return this.repo.show;
  }
  get shows(): Show[] {
    return this.repo.shows;
  }

  compareShows(s1: Show, s2: Show) {
    return s1 && s2 && s1.movie.title == s2.movie.title && s1.id == s2.id;
  }
  compareStrings(s1: string, s2: string) {//used to compare users that are stored as string in bookings
    return s1 && s2 && s1 == s2;
  }

  ngOnInit() {
    this.isAuthenticated = this.authorizeService.isAuthenticated();
    this.authorizeService.getUser().pipe(map(u => u && u.name )).subscribe( data => this.active_user = data);//assign logged in username to active_user
    this.setCurrentUser(this.active_user);
    this.repo.getBookings();
    this.repo.getShows();
   }
}

