import { Component, OnInit } from '@angular/core';
import { MovieRepository } from 'src/app/Repositories/movie.repository';
import { Movie } from 'src/app/models/movie.model';

@Component({
  selector: 'admin-manage-movies',
  templateUrl: './manage-movies.component.html',
  styleUrls: ['./manage-movies.component.css']
})
export class ManageMoviesComponent implements OnInit {

  constructor(private repo: MovieRepository) { }

  tableMode: boolean = true;

  selectMovie(id: number) {
      this.repo.getMovie(id);
    }
    saveMovie() {
        if (this.repo.movie.id == null) {
            this.repo.createMovie(this.repo.movie);
        } else {
            this.repo.replaceMovie(this.repo.movie);
        }
        this.clearMovie()
        this.tableMode = true;
    }
    deleteMovie(id: number) {
        this.repo.deleteMovie(id);
    }
    clearMovie() {
        this.repo.movie = new Movie();
        this.tableMode = true;
    }
    get movies(): Movie[] {
        return this.repo.movies;
    }
    get movie(): Movie {
    return this.repo.movie;
    }

  ngOnInit() {
  }

}
