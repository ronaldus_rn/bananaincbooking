import { Component, OnInit } from '@angular/core';
import { Movie } from 'src/app/models/movie.model';
import { Show } from 'src/app/models/show.model';
import { MovieRepository } from 'src/app/Repositories/movie.repository';
import { TimeSlot } from 'src/app/models/timeslot.model';
import { Cinema } from 'src/app/models/cinema.model';

import {FormControl} from '@angular/forms';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

import * as _moment from 'moment';


const moment =  _moment;

@Component({
  selector: 'admin-edit-shows',
  templateUrl: './edit-shows.component.html',
  styleUrls: ['./edit-shows.component.css']
})
export class EditShowsComponent implements OnInit {

  constructor(private repo: MovieRepository) { }
  // testy = Date.now();

  get movie(): Movie {
    return this.repo.movie;
  }
  get movies(): Movie[] {
    return this.repo.movies;
  }

  get timeslot(): TimeSlot {
    return this.repo.timeslot;
  }
  get timeslots(): TimeSlot[] {
    return this.repo.timeslots;
  }

  get dates(): _moment.Moment[] {
    return this.repo.dates_moment;
  }

  get cinema(): Cinema {
    return this.repo.cinema;
  }
  get cinemas(): Cinema[] {
    return this.repo.cinemas;
  }

  get show(): Show {
    return this.repo.show;
  }
  get shows(): Show[] {
    return this.repo.shows;
  }

  get date(): _moment.Moment
  {
    return this.repo.date_moment;
  }
  compareMovies(m1: Movie, m2: Movie) {
    return m1 && m2 && m1.title == m2.title;
  }

  compareTimeSlots(t1: TimeSlot, t2: TimeSlot) {
    return t1 && t2 && t1.time == t2.time;
  }

  compareDates(d1: Date, d2: Date) {
    return d1 && d2 && d1 == d2;
  }

  compareCinemas(c1: Cinema, c2: Cinema) {
    return c1 && c2 && c1.id == c2.id;
  }

  // myFilter = (d: Date): boolean => {
  //   const day = d.getDay();
  //   // Prevent Saturday and Sunday from being selected.
  //   return day !== 0 && day !== 6;
  // }

  ngOnInit() {
  }

}
