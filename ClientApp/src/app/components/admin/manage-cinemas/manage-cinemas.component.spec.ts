import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageCinemasComponent } from './manage-cinemas.component';

describe('ManageCinemasComponent', () => {
  let component: ManageCinemasComponent;
  let fixture: ComponentFixture<ManageCinemasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageCinemasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageCinemasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
