import { Component, OnInit } from '@angular/core';
import { Cinema } from 'src/app/models/cinema.model';
import { MovieRepository } from 'src/app/Repositories/movie.repository';

@Component({
  selector: 'admin-manage-cinemas',
  templateUrl: './manage-cinemas.component.html',
  styleUrls: ['./manage-cinemas.component.css']
})
export class ManageCinemasComponent implements OnInit {

  constructor(private repo: MovieRepository) { }

  tableMode: boolean = true;
  
  get cinema(): Cinema {
      return this.repo.cinema;
  }

  selectCinema(id: number) {
      this.repo.getCinema(id);
  }
  
  saveCinema() {
      if (this.repo.cinema.id == null) {
          this.repo.createCinema(this.repo.cinema);
      } else {
          this.repo.replaceCinema(this.repo.cinema);
      }
      this.clearCinema()
      this.tableMode = true;
  }
  deleteCinema(id: number) {
      this.repo.deleteCinema(id);
  }
  clearCinema() {
      this.repo.cinema = new Cinema();
      this.tableMode = true;
  }
  get cinemas(): Cinema[] {
      return this.repo.cinemas;
  }
  ngOnInit() {
    this.repo.getCinemas();
  }

}
