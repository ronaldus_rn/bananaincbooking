import { Component, OnInit } from '@angular/core';
import { MovieRepository } from 'src/app/Repositories/movie.repository';
import { Movie } from 'src/app/models/movie.model';

@Component({
  selector: 'admin-edit-movies',
  templateUrl: './edit-movies.component.html',
  styleUrls: ['./edit-movies.component.css']
})
export class EditMoviesComponent implements OnInit {

  constructor(private repo: MovieRepository) { }
  
  get movie(): Movie {
    return this.repo.movie;
}

  ngOnInit() {
  }

}
