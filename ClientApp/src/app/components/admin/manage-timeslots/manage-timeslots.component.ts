import { Component, OnInit } from '@angular/core';
import { MovieRepository } from 'src/app/Repositories/movie.repository';
import { TimeSlot } from 'src/app/models/timeslot.model';

@Component({
  selector: 'admin-manage-timeslots',
  templateUrl: './manage-timeslots.component.html',
  styleUrls: ['./manage-timeslots.component.css']
})
export class ManageTimeslotsComponent implements OnInit {

  constructor(private repo: MovieRepository) { }
  tableMode: boolean = true;
  get timeSlot(): TimeSlot {
      return this.repo.timeslot;
  }
  selectTimeSlot(id: number) {
      this.repo.getTimeSlot(id);
  }
  saveTimeSlot() {
      if (this.repo.timeslot.id == null) {
          this.repo.createTimeSlot(this.repo.timeslot);
      } else {
          this.repo.replaceTimeSlot(this.repo.timeslot);
      }
      this.clearTimeSlot()
      this.tableMode = true;
  }
  deleteTimeSlot(id: number) {
      this.repo.deleteTimeSlot(id);
  }
  clearTimeSlot() {
      this.repo.timeslot = new TimeSlot();
      this.tableMode = true;
  }
  get timeSlots(): TimeSlot[] {
      return this.repo.timeslots;
  }

  ngOnInit() {
      this.repo.getTimeSlots();
  }

}
