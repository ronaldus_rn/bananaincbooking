import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageTimeslotsComponent } from './manage-timeslots.component';

describe('ManageTimeslotsComponent', () => {
  let component: ManageTimeslotsComponent;
  let fixture: ComponentFixture<ManageTimeslotsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageTimeslotsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageTimeslotsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
