import { Component, OnInit } from '@angular/core';
import { MovieRepository } from 'src/app/Repositories/movie.repository';
import { Booking } from 'src/app/models/booking.model';

@Component({
  selector: 'admin-manage-bookings',
  templateUrl: './manage-bookings.component.html',
  styleUrls: ['./manage-bookings.component.css']
})
export class ManageBookingsComponent implements OnInit {

  constructor(private repo: MovieRepository) { }

  tableMode: boolean = true;


  selectBooking(id: number) {
    this.repo.getBooking(id);
  }
  saveBooking() {
    if (this.repo.booking.id == null) {
      this.repo.createBooking(this.repo.booking,null);
    } else {
      this.repo.replaceBooking(this.repo.booking);
    }
    this.clearBooking()
    this.tableMode = true;
  }
  deleteBooking(id: number) {
    this.repo.deleteBooking(id);
  }
  clearBooking() {
    this.repo.booking = new Booking();
    this.tableMode = true;
  }

  get booking(): Booking {
    return this.repo.booking;
  }
  get bookings(): Booking[] {
    return this.repo.bookings;
  }

  get users(): string[] {
    return this.repo.users_str
  }
  get currentUser(): string {
    return this.repo.bookingfilter.user;
  }
  setCurrentUser(newUser: string){
    this.repo.bookingfilter.user = newUser;
    this.repo.getBookings();
  }

  ngOnInit() {
    this.repo.getShows(true);
    this.repo.getMovies();
    this.repo.getCinemas();
    this.repo.getTimeSlots();    
    this.repo.getBookings();    
  }

}
