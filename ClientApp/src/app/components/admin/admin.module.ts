import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule }    from '@angular/forms';

import { AdminRoutingModule } from './admin-routing.module';
import { ManageMoviesComponent } from './manage-movies/manage-movies.component';
import { ManageBookingsComponent } from './manage-bookings/manage-bookings.component';
import { AdminComponent } from './admin/admin.component';
import { AdminNavComponent } from './admin-nav/admin-nav.component';
import { EditMoviesComponent } from './edit-movies/edit-movies.component';
import { ManageCinemasComponent } from './manage-cinemas/manage-cinemas.component';
import { EditCinemasComponent } from './edit-cinemas/edit-cinemas.component';
import { ManageTimeslotsComponent } from './manage-timeslots/manage-timeslots.component';
import { EditTimeslotsComponent } from './edit-timeslots/edit-timeslots.component';
import { EditBookingsComponent } from './edit-bookings/edit-bookings.component';
import { EditShowsComponent } from './edit-shows/edit-shows.component';
import { ManageShowsComponent } from './manage-shows/manage-shows.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { DemoMaterialModule } from 'src/app/material-module';
import { MatNativeDateModule } from '@angular/material/core';



@NgModule({

  declarations: [    
    AdminComponent,
    ManageMoviesComponent,
    ManageBookingsComponent,
    AdminNavComponent,
    EditMoviesComponent,
    ManageCinemasComponent,
    EditCinemasComponent,
    ManageTimeslotsComponent,
    EditTimeslotsComponent,
    EditBookingsComponent,
    EditShowsComponent,
    ManageShowsComponent,
  ],

  imports: [
    CommonModule,
    FormsModule,
    AdminRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    DemoMaterialModule,
    MatNativeDateModule,
    ReactiveFormsModule,    
  ]
})
export class AdminModule { }
