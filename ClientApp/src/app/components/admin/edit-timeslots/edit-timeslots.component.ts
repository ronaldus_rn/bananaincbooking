import { Component, OnInit } from '@angular/core';
import { MovieRepository } from 'src/app/Repositories/movie.repository';
import { TimeSlot } from 'src/app/models/timeslot.model';

@Component({
  selector: 'admin-edit-timeslots',
  templateUrl: './edit-timeslots.component.html',
  styleUrls: ['./edit-timeslots.component.css']
})
export class EditTimeslotsComponent implements OnInit {

  constructor(private repo: MovieRepository) { }

  get timeSlot(): TimeSlot {
    return this.repo.timeslot;
  }

  ngOnInit() {
  }

}
