import { Component, OnInit } from '@angular/core';
import { Show } from 'src/app/models/show.model';
import { MovieRepository } from 'src/app/Repositories/movie.repository';
import { Booking } from 'src/app/models/booking.model';

@Component({
  selector: 'admin-edit-bookings',
  templateUrl: './edit-bookings.component.html',
  styleUrls: ['./edit-bookings.component.css']
})
export class EditBookingsComponent implements OnInit {

  constructor(private repo: MovieRepository) { }

  get booking(): Booking {
    return this.repo.booking;
  }
  get bookings(): Booking[] {
    return this.repo.bookings;
  }
  // get user(): Booking {
  //   return this.repo.booking;
  // }
  get users(): string[] {
    return this.repo.users_str;
  }

  get show(): Show {
    return this.repo.show;
  }
  get shows(): Show[] {
    return this.repo.shows;
  }

  compareShows(s1: Show, s2: Show) {
    return s1 && s2 && s1.movie.title == s2.movie.title && s1.id == s2.id;
  }
  compareStrings(s1: string, s2: string) {//used to compare users that are stored as string in bookings
    return s1 && s2 && s1 == s2;
  }

  ngOnInit() {
  }

}
