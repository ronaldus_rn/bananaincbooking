import { Component, OnInit } from '@angular/core';
import { AuthorizeService } from '../../../../api-authorization/authorize.service';
import { MovieRepository } from 'src/app/Repositories/movie.repository';
import { Movie } from 'src/app/models/movie.model';
import { Cinema } from 'src/app/models/cinema.model';
import { TimeSlot } from 'src/app/models/timeslot.model';
import { Show } from 'src/app/models/show.model';
import { map, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Booking } from 'src/app/models/booking.model';



@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private repo: MovieRepository, private authorizeService: AuthorizeService) { 
  }
  
  get movies():Movie[]{
    return this.repo.movies;
  }
  get bookings():Booking[]{
    return this.repo.bookings;
  }
  get cinemas():Cinema[]{
    return this.repo.cinemas;
  }
  get timeslots():TimeSlot[]{
    return this.repo.timeslots;
  }
  get shows():Show[]{
    return this.repo.shows;
  }

  public isAuthenticated: Observable<boolean>;
  public userName: Observable<string>;

  ngOnInit() {
    this.repo.moviefilter.reset();
    this.repo.bookingfilter.reset();
    this.repo.showfilter.reset();
    this.repo.showfilter_day.reset();
    this.repo.getMovies();
    this.repo.getCinemas();
    this.repo.getTimeSlots();
    this.isAuthenticated = this.authorizeService.isAuthenticated();
    this.userName = this.authorizeService.getUser().pipe(map(u => u && u.name));    
    this.repo.getBookings();
    this.repo.getShows(true);
    

  }

}
