import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManageMoviesComponent } from './manage-movies/manage-movies.component';
import { ManageBookingsComponent } from './manage-bookings/manage-bookings.component';
// import { OverviewComponent } from './overview/overview.component';
import { AdminComponent } from './admin/admin.component';
import { ManageCinemasComponent } from './manage-cinemas/manage-cinemas.component';
import { ManageTimeslotsComponent } from './manage-timeslots/manage-timeslots.component';
import { ManageShowsComponent } from './manage-shows/manage-shows.component';

// import { AuthGuard }                from '../auth/auth.guard';
// import { AuthorizeGuard } from 'src/api-authorization/authorize.guard';
const adminRoutes: Routes = [
  { path: 'admin',  component: AdminComponent },
  { path: 'admin/movies', component: ManageMoviesComponent },
  { path: 'admin/bookings', component: ManageBookingsComponent },
  { path: 'admin/cinemas',  component: ManageCinemasComponent },
  { path: 'admin/timeslots',  component: ManageTimeslotsComponent },
  { path: 'admin/shows',  component: ManageShowsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(adminRoutes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
