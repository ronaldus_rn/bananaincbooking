import { Component, OnInit } from '@angular/core';
import { MovieRepository } from 'src/app/Repositories/movie.repository';
import { Show } from 'src/app/models/show.model';
import { Movie } from 'src/app/models/movie.model';

import {FormControl} from '@angular/forms';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

import * as _moment from 'moment';
import { Cinema } from 'src/app/models/cinema.model';
import { forEach } from '@angular/router/src/utils/collection';
import { TimeSlot } from 'src/app/models/timeslot.model';


const moment =  _moment;

@Component({
    selector: 'admin-manage-shows',
    templateUrl: './manage-shows.component.html',
    styleUrls: ['./manage-shows.component.css']
})
export class ManageShowsComponent implements OnInit {

    constructor(private repo: MovieRepository) { }
    tableMode: boolean = true;
    

    

    selectShow(id: number) {
        this.repo.getShow(id);
    }
    saveShow() {
        if (this.repo.show.id == null) {
            this.repo.show.date = this.repo.date_moment.valueOf();
            this.repo.createShow(this.repo.show);
        } else {
            this.repo.show.date = this.repo.date_moment.valueOf();
            this.repo.replaceShow(this.repo.show);
        }
        this.clearShow()
        // this.tableMode = true;
    }
    deleteShow(id: number) {
        this.repo.deleteShow(id);
    }

    clearShow() {
        this.repo.show = new Show();
        this.tableMode = true;
    }

    createShowsForThisMonth(){

        var startofM= moment(0,"HH");//or today, no use creating shows in the past
        var currentDay = moment(startofM);
        var endofM = moment(startofM).endOf("M");

        while (currentDay.valueOf() < endofM.valueOf()) {
            this.repo.timeslots.forEach(timeslot => {
                this.repo.cinemas.forEach(cinema => {
                    this.repo.show = new Show();
                    this.repo.show.movie = null,
                    this.repo.show.bookings = null,
                    this.repo.show.timeslot = timeslot,
                    this.repo.show.date = currentDay.valueOf(),
                    this.repo.show.cinema = cinema
                    this.repo.createShow(this.repo.show);

                });
                
            });
            currentDay.add(1,"d");   
        }
        this.repo.getShows(true);
        this.clearShow();
    }

    createShowsForNextMonth(){

        var temp= moment().endOf("M");//end of this month, end of day
        var startofM = moment(temp).add(1,'h');

        var currentDay = moment(startofM);
        var endofM = moment(startofM).endOf("M");

        while (currentDay.valueOf() < endofM.valueOf()) {
            this.repo.timeslots.forEach(timeslot => {
                this.repo.cinemas.forEach(cinema => {
                    this.repo.show = new Show();
                    this.repo.show.movie = null,
                    this.repo.show.bookings = null,
                    this.repo.show.timeslot = timeslot,
                    this.repo.show.date = currentDay.valueOf(),
                    this.repo.show.cinema = cinema
                    this.repo.createShow(this.repo.show);

                });
                
            });
            currentDay.add(1,"d");   
        }
        this.repo.getShows(true);
        this.clearShow();
    }    
    get show(): Show {
        return this.repo.show;
    }
    get shows(): Show[] {
        return this.repo.shows;
    }

    get cinemas(): string[]{
        return this.repo.cinemas_str;
    }
    get timeslots(): string[]{
        return this.repo.times_str;
    }
    get dates(): _moment.Moment[]{
        return this.repo.dates_moment;
    }
    setDate(d: _moment.Moment = null){
        if (d != null)
        {
            // d.hour(1);
            this.repo.date_moment = d;//to ensure all moments are created with samr time, for filtering shows by date;
            // this.repo.moment_date.hour(1);
        }
        else
        {
            this.repo.date_moment = moment(0, "HH");
            // this.repo.moment_date = moment().hours(2);
        }
    }
    setCinemaFilter(newCinema: number) {
        this.repo.showfilter_day.cinema = newCinema;
        // this.setCurrentDay(null);
        this.repo.getShowsOnDay(true);
      }
      setDayFilter(day_val: number){//for shows on certain days
        this.repo.showfilter_day.start_date = day_val;
        this.repo.showfilter_day.end_date = day_val;
        this.repo.date_moment = moment(day_val);
        this.repo.getShowsOnDay(true);
      }
    
    ngOnInit() {
        this.repo.getShows(true);
        this.repo.getMovies();
        // this.repo.date = moment().format('LL');
        // this.testy.valueOf()
        this.repo.getCinemas();
        this.repo.getTimeSlots();
    }

}
