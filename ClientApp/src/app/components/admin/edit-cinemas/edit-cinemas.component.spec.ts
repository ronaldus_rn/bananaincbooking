import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCinemasComponent } from './edit-cinemas.component';

describe('EditCinemasComponent', () => {
  let component: EditCinemasComponent;
  let fixture: ComponentFixture<EditCinemasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCinemasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCinemasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
