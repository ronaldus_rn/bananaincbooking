import { Component, OnInit } from '@angular/core';
import { Cinema } from 'src/app/models/cinema.model';
import { MovieRepository } from 'src/app/Repositories/movie.repository';

@Component({
  selector: 'admin-edit-cinemas',
  templateUrl: './edit-cinemas.component.html',
  styleUrls: ['./edit-cinemas.component.css']
})
export class EditCinemasComponent implements OnInit {

  constructor(private repo: MovieRepository) { }

  get cinema(): Cinema {
    return this.repo.cinema;
}

  ngOnInit() {
  }

}
