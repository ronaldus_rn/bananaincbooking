import { Component, OnInit } from '@angular/core';
import { MovieRepository, showFilter } from 'src/app/Repositories/movie.repository';
import { Show } from 'src/app/models/show.model';
// import { DatePipe } from '@angular/common';
import { formatDate } from '@angular/common';
import { Booking } from 'src/app/models/booking.model';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthorizeService } from 'src/api-authorization/authorize.service';


import * as _moment from 'moment';


const moment = _moment;

@Component({
  selector: 'app-list-shows',
  templateUrl: './list-shows.component.html',
  styleUrls: ['./list-shows.component.css'],
  // providers: [DatePipe]
})
export class ListShowsComponent implements OnInit {

  bookingtable: boolean = false;

  public isAuthenticated: Observable<boolean>;//will move to repo
  public active_user: string;//will move to repo
  constructor(private repo: MovieRepository, private authorizeService: AuthorizeService) {

  }

  //movie filter
  get movies(): string[] {
    return this.repo.movies_str
  }
  get currentMovie(): string {
    return this.repo.showfilter.movie;
  }
  setCurrentMovie(newMovie: string) {
    this.repo.showfilter.movie = newMovie;
    // this.setCurrentDay(null);
    this.repo.getShows();
  }

  // // Date filter
  get dates(): _moment.Moment[] {
    return this.repo.dates_moment;
  }
  //   get dates(): Date[] {
  //     return this.repo.dates;
  //   }
    get currentDate(): _moment.Moment {
      return this.repo.date_moment;
    }
    setDayFilter(day_val: number){//for shows on certain days
      this.repo.showfilter_day.start_date = day_val;
      this.repo.showfilter_day.end_date = day_val;
      this.repo.date_moment = moment(day_val);
      this.repo.getShowsOnDay();
    }
    setStartDateFilter( date_val: number)//for filtering out pas dates
    {
      this.repo.showfilter.start_date = date_val;
      this.repo.getShows();
    }
  // Time filter
  get times(): string[] {
    return this.repo.times_str;
  }
  get currentTime(): string {
    return this.repo.showfilter.time;
  }
  setCurrentTime(newTime: string) {
    this.repo.showfilter.time = newTime;
    this.repo.getShows();
  }
  // booking tools
  selectBooking(id: number) {
    this.repo.getBooking(id);
  }
  saveBooking() {
    if (this.repo.booking.id == null) {
      this.repo.createBooking(this.repo.booking, this.active_user);
    } else {
      this.repo.replaceBooking(this.repo.booking);
    }
    this.clearBooking()
    this.bookingtable = false;

  }
  deleteBooking(id: number) {
    this.repo.deleteBooking(id);
  }
  clearBooking() {
    this.repo.booking = new Booking();
    this.bookingtable = false;
  }
  setShowForBooking(sh: Show) {
    this.repo.booking.show = sh;
  }
  get booking(): Booking {
    return this.repo.booking;
  }
  get bookings(): Booking[] {
    return this.repo.bookings;
  }
  // saveBooking() {
  //   if (this.repo.booking.id == null) {
  //     this.repo.createBooking(this.repo.booking,null);
  //   } else {
  //     this.repo.replaceBooking(this.repo.booking);
  //   }
  //   this.clearBooking()
  //   this.bookingtable = true;
  // }
  // deleteBooking(id: number) {
  //   this.repo.deleteBooking(id);
  // }
  // clearBooking() {
  //   this.repo.booking = new Booking();
  //   this.bookingtable = true;
  // }
  // get booking(): Booking {
  //   return this.repo.booking;
  // }
  // get bookings(): Booking[] {
  //   return this.repo.bookings;
  // }

  // users
  get users(): string[] {
    return this.repo.users_str
  }

  get currentUser(): string {
    return this.repo.bookingfilter.user;
  }
  setCurrentUser(newUser: string) {
    this.repo.bookingfilter.user = newUser;
    this.repo.getBookings();
  }

  get shows(): Show[] {
    return this.repo.shows;
  }



  compareStrings(s1: string, s2: string) {//used to compare users that are stored as string in bookings
    return s1 && s2 && s1 == s2;
  }

  compareShows(s1: Show, s2: Show) {
    return s1 && s2 && s1.movie.title == s2.movie.title && s1.id == s2.id;
  }

  ngOnInit() {
    // this.todaydate = this.datePipe.transform(this.todaydate,'yyyy-MM-dd');
    // var tod = formatDate(this.todaydate,'yyyy/MM/dd',"en-ZA");
    // var d = Number(formatDate(this.todaydate,'dd',"en-ZA"));
    // var m = Number(formatDate(this.todaydate,'LL',"en-ZA"));
    // var y = Number(formatDate(this.todaydate,'yyyy',"en-ZA"));

    // this.otdate = 1563461504063;

    // this.repo.getShows();
    // this.repo.getShows(true);
    // this.setCurrentDay(this.days[0]);
    this.isAuthenticated = this.authorizeService.isAuthenticated();
    this.authorizeService.getUser().pipe(map(u => u && u.name)).subscribe(data => this.active_user = data);//assign logged in username to active_user
    this.setCurrentUser(this.active_user);
    this.repo.getBookings();
    this.setStartDateFilter(moment(0 , 'HH').valueOf());
    this.setDayFilter(moment(0, "HH").valueOf());//set filter to todays date
    // if (this.shows.length == 0) {
      this.repo.getShows();
    //   this.setDayFilter(this.shows[0].date.valueOf());
    // }
    // this.repo.getShows(true);
    // this.repo.shows = [];
    // this.repo.days_num.sort((a,b) => a.localeCompare(b));
    // this.repo.days_num.sort((a,b) => a - b);
    // this.ShowsOnDay(1,1,1);
  }

}
