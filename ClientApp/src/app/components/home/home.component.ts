import { Component } from '@angular/core';
import { MovieRepository } from "../../Repositories/movie.repository";
import { Movie } from "../../models/movie.model";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent {
    placeholderUrl = "https://img.moviepostershop.com/the-benchwarmers-movie-poster-2006-1020340578.jpg"
    constructor(private repo: MovieRepository){}
    
    get movie():Movie{
      return this.repo.movie;
    }  
    get movies():Movie[]{
      return this.repo.movies;
    }


}
