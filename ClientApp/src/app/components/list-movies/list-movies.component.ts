import { Component, OnInit } from '@angular/core';
import { MovieRepository } from "../../Repositories/movie.repository";
import { Movie } from "../../models/movie.model";
import * as _moment from 'moment';


const moment = _moment;
@Component({
  selector: 'app-list-movies',
  templateUrl: './list-movies.component.html',
  styleUrls: ['./list-movies.component.css']
})
export class ListMoviesComponent implements OnInit {

  constructor(private repo: MovieRepository) { 
    
  }
  get genres(): string[] {
    return this.repo.genres
  }
  get currentGenre(): string {
    return this.repo.moviefilter.genre;
  }
  setCurrentGenre(newGenre: string){
    this.repo.moviefilter.genre = newGenre;
    this.repo.getMovies();
  }
  //filter shows by movie
  get movies_str(): string[] {
    return this.repo.movies_str
  }
  get currentMovie(): string {
    return this.repo.showfilter.movie;
  }
  // setCurrentMovie(newMovie: string){//for booking
  //   this.repo.showfilter.movie = newMovie;
  //   this.repo.getShows();
  // }


  setMovieForBooking(newMovie: string){//for booking
    this.repo.showfilter.movie = newMovie;
    this.repo.getShows();//generates list of available days in month,year
    // this.repo.moment_date = moment(this.repo.shows[0].date);
    // this.setCurrentDay(15);
  }

    // setDayFilter(day_val: number){
    //   this.repo.showfilter_day.start_date = day_val;
    //   this.repo.showfilter_day.end_date = day_val;
    //   this.repo.moment_date = moment(day_val);
    //   this.repo.getShows(true);
    // }


  get movies():Movie[]{
    return this.repo.movies;
  }
  
  ngOnInit() {
    // this.repo.getShows();
    this.repo.moviefilter.reset();
    this.repo.showfilter.reset();
    this.repo.showfilter_day.reset();
    this.repo.getMovies();


  }

}

