Allegiance Technical Challenge

Problem Statement:    build a movie booking site that allows users to make bookings (no payments needed) and allows an admin user to add new films.
    
Users should create accounts and be able to book multiple films over different days and show times.    
Users should be able to view all bookings on a Your Bookings page.    
The home page should show a carousel style of new movies.    
There should be 4 pages in total: Home, Movies, Your Bookings and Admin.    

Project Specifications:

Angular 7 for frontend
Entity framework core with SQLite for integration
ASP.Net core 3.0preview5

Roadmap

#Done:

#create ASP.net Angular app with user registration (using IdentityServe)
this was done using the angular project template with individual authentication
#Set up Movie database and models required for uploading/viewing and editing movies
Create models for db on the asp.net side    
create seed data and innitialise    
create controllers and test json returned    
create models on agular side to dissplay moviedata    
#add Home page with Carousel of movies
a movies carousel on the home page that displays movies
#add Admin page to add/edit/view movies,shows etc.
#User must be able to create a booking
#User must be able to view/edit bookings

#ToDo:

#streamline Booking process

#Add admin privileges so that only users with admin privileges  can access the Admin page
#Scaffold identity into an MVC to customise templates for user authentication settings.
I experinced issues when trying to scaffold identity, seems to be a asp.net core 3.0 bug
#-possibly: add Session data cache.